-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 19, 2019 at 06:47 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

CREATE TABLE `document` (
  `Document_id` int(11) NOT NULL,
  `ref_student_id` int(11) NOT NULL,
  `Document_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `document`
--

INSERT INTO `document` (`Document_id`, `ref_student_id`, `Document_name`) VALUES
(2, 2, '[\'file1.png\',\'file2.png\',\'file3.png\']');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `Student_id` int(11) NOT NULL,
  `Student_fname` varchar(100) NOT NULL,
  `Student_lname` varchar(100) NOT NULL,
  `Student_pname` varchar(100) NOT NULL,
  `Student_mobile` int(10) NOT NULL,
  `Student_standard` int(11) NOT NULL,
  `Student_course` varchar(250) NOT NULL,
  `Student_email` varchar(100) NOT NULL,
  `Student_api` text NOT NULL,
  `Student_datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`Student_id`, `Student_fname`, `Student_lname`, `Student_pname`, `Student_mobile`, `Student_standard`, `Student_course`, `Student_email`, `Student_api`, `Student_datetime`) VALUES
(2, 'Chintan', 'Dhameliya', 'Dineshbhai', 1234567890, 12, 'math', '', '123456', '2019-12-18 09:38:21'),
(3, 'Chintan', 'Dhameliya', 'Dineshbhai', 1234567890, 12, 'math', '', '123456', '2019-12-18 09:13:05'),
(4, 'Chintan', 'Dhameliya', 'Dineshbhai', 1234567890, 12, 'math', '', '123456', '2019-12-18 09:36:53'),
(5, 'Chintan', 'Dhameliya', 'Dineshbhai', 1234567890, 12, 'math', '', '123456', '2019-12-18 09:37:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`Document_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`Student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `document`
--
ALTER TABLE `document`
  MODIFY `Document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `Student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
